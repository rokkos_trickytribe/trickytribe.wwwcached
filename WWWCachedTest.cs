﻿using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using TrickyTribe.AsyncAwaitUtil;

namespace TrickyTribe.Parser
{
    public class WWWCachedTest : MonoBehaviour
    {

        void Start()
        {

            WWWCached.EnableLogging = false;
            WWWCached.FolderName = "cached";
            WWWCached.ClearCacheAll();


            TestAsync();
            /*  StartCoroutine( TestCoroutine() );
              TestCallback();
              TestMultipleSequential();
              TestMultipleParallel();*/
        }

        // use async/await
        async void TestAsync()
        {
            Debug.Log("TestAsync");
            var data = await new WWWCached("https://tifm.trickytribe.tfsnw.net/googleproxy/elevator.php?sheet=LEVELS",
                true, "levels").ExecuteAsync();
            Debug.Log("TestAsync Done\n" + data);
        }

        // use coroutine
        IEnumerator TestCoroutine()
        {
            Debug.Log("TestCoroutine");
            var www = new WWWCached("https://tifm.trickytribe.tfsnw.net/googleproxy/elevator.php?sheet=LEVELS", true,
                "levels");
            yield return www.ExecuteAsync().AsIEnumerator();
            Debug.Log("TestCoroutine Done\n" + www.Result);
        }


        void TestCallback()
        {
            Debug.Log("TestCallback");
            var www = new WWWCached("https://tifm.trickytribe.tfsnw.net/googleproxy/elevator.php?sheet=LEVELS", true,
                "levels");
            www.OnCompleted.AddListener(OnCompleted);
            www.Execute();
        }

        void OnCompleted(WWWCached www)
        {
            Debug.Log(www.Result);
            Debug.Log("TestCallback Done");
        }


        // download multiple files in sequence
        async void TestMultipleSequential()
        {
            var time = Time.realtimeSinceStartup;
            Debug.Log("TestMultipleSequential");

            var levels = await new WWWCached("https://tifm.trickytribe.tfsnw.net/googleproxy/elevator.php?sheet=LEVELS",
                true, "levels").ExecuteAsync();
            var characters =
                await new WWWCached("https://tifm.trickytribe.tfsnw.net/googleproxy/elevator.php?sheet=CHARACTERS",
                    true, "characters").ExecuteAsync();

            Debug.Log($"TestMultipleSequential Done ({Time.realtimeSinceStartup - time})\n{levels}\n{characters}");
        }


        // download multiple files simultaneous
        async void TestMultipleParallel()
        {
            var time = Time.realtimeSinceStartup;
            Debug.Log("TestMultipleParallel");

            var levelsTask = new WWWCached("https://tifm.trickytribe.tfsnw.net/googleproxy/elevator.php?sheet=LEVELS",
                true, "levels").ExecuteAsync();
            var charactersTask =
                new WWWCached("https://tifm.trickytribe.tfsnw.net/googleproxy/elevator.php?sheet=CHARACTERS", true,
                    "characters").ExecuteAsync();

            await Task.WhenAll(levelsTask, charactersTask);

            Debug.Log(
                $"TestMultipleParallel Done ({Time.realtimeSinceStartup - time})\n{levelsTask.Result}\n{charactersTask.Result}");
        }
    }
}