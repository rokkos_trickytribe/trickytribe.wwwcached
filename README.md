# WWWCached
This is a library for parsing data from Google Sheets.
This enables game designers and artists to change runtime parameters very easily.

## Setup
1. Add this repository as a submodule in your project under Assets/Libraries/TrickyTribe.WWWCached
2. Add dependencies submodules to the project 
	* [TrickyTribe.Collections]()
	* [TrickyTribe.AsyncAwaitUtil]()
3. Create a Google sheet following this [template](https://docs.google.com/spreadsheets/d/1Eyrd-toiZeSqDOUw7yLzd7W1iuMDYhJ6L_jKw-fAPBQ/edit?usp=sharing)
4. Create Resource folder under Unity Project
5. Create TSVSettings.asset  in Resource folder (right-click -> Create -> TSV -> TSVSettings)
6. Put in Sheet id from your Google Sheet
7. Share this Google Sheet with the account: elevatorproxy@elevator-f4826.iam.gserviceaccount.com and give him Viewer permission only.

## Usage
1. Edit Google Sheet
2. Click TrickyTribe->TSV->Download Data

## Notes
Dev flag is always true. If it's not you can manually edit cached TSV data