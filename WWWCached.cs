﻿using UnityEngine;
using TrickyTribe.AsyncAwaitUtil;
using webResult = UnityEngine.Networking.UnityWebRequest.Result;


namespace TrickyTribe.Parser
{
    public class WWWCachedEvent : UnityEngine.Events.UnityEvent<WWWCached>
    {
    }

    public class WWWCached
    {
        /// <summary>
        ///   <para>Event invoked when download/load completes.</para>
        /// </summary>
        public WWWCachedEvent OnCompleted = new WWWCachedEvent();

        static string SHA1(string message)
        {
            return System.BitConverter.ToString(
                new System.Security.Cryptography.SHA1CryptoServiceProvider().ComputeHash(
                    System.Text.Encoding.UTF8.GetBytes(message)
                )
            ).ToLower().Replace("-", "");
        }

        /// <summary>
        ///   <para>Removes cache folder and all the content</para>
        /// </summary>
        public static void ClearCacheAll()
        {
            try
            {
                var path = System.IO.Path.Combine(Application.isEditor ? Application.dataPath : Application.temporaryCachePath,
                    FolderName);

                if (System.IO.Directory.Exists(path))
                    System.IO.Directory.Delete(path, true);
            }
            catch (System.Exception e)
            {
                LogWarning($"Error deleting cache folder\n{e.Message}");
            }
        }

        /// <summary>
        ///   <para>Enable or disable logging</para>
        /// </summary>
        public static bool EnableLogging = true;


        /// <summary>
        ///   <para>Root folder name where cached files will be stored (in Editor under Application.dataPath, on devices under Application.temporaryCachePath</para>
        /// </summary>
        public static string FolderName = "cached";


        /// <summary>
        ///   <para>How old can cached file get before discarded</para>
        /// </summary>
        public System.TimeSpan CacheTimeout;

        /// <summary>
        ///   <para>Timeout for http request</para>
        /// </summary>
        public int DownloadTimeout = 15;


        /// <summary>
        ///   <para>Holds result of download/load</para>
        /// </summary>
        public string Result { get; private set; }


        private UnityEngine.Networking.UnityWebRequest www;
        private string url;
        private string embeddedResource;
        private string cacheDirectory;
        private string file;
        private bool enableCached;
        private bool enableDownload;


        public WWWCached(string url, bool enableCached = true, string embeddedResource = null, bool enableDownload = true)
        {
            this.url = url;
            this.enableCached = enableCached;
            this.embeddedResource = embeddedResource;
            this.enableDownload = enableDownload;

            CacheTimeout = System.TimeSpan.FromDays(1);
            cacheDirectory = System.IO.Path.Combine(Application.isEditor ? Application.dataPath : Application.temporaryCachePath,
                FolderName);
#if UNITY_EDITOR
            file = System.IO.Path.Combine(cacheDirectory, embeddedResource + ".txt");
#else
                file = System.IO.Path.Combine(cacheDirectory, SHA1(url) );
#endif

        }

        /// <summary>
        ///   <para>Check if cached file exists</para>
        /// </summary>
        public bool HasCache()
        {
            return System.IO.File.Exists(file);
        }

        /// <summary>
        ///   <para>Remove cached file</para>
        /// </summary>
        public void ClearCache()
        {
            if (!System.IO.File.Exists(file)) return;
            try
            {
                System.IO.File.Delete(file);
            }
            catch (System.Exception e)
            {
                LogWarning($"Error deleting cache file '{file}' ({url}):\n{e.Message}");
            }
        }



        void SaveToCache(string data)
        {
            if (string.IsNullOrEmpty(data))
                return;

            if (string.IsNullOrEmpty(file))
                return;
            try
            {
                Log("Saving to cache file: " + file);
                // create required folders
                new System.IO.FileInfo(file).Directory.Create();
                // write to disk
                System.IO.File.WriteAllText(file, data);
                System.IO.File.SetCreationTimeUtc(file, System.DateTime.UtcNow);

#if UNITY_EDITOR
                UnityEditor.AssetDatabase.Refresh();
#endif
            }
            catch (System.Exception e)
            {
                LogWarning($"Error saving to cache file '{file}' ({url}):\n{e.Message}");
            }
        }


        async System.Threading.Tasks.Task<string> Download()
        {
            string data;
            using (www = UnityEngine.Networking.UnityWebRequest.Get(url))
            {
                Log("Downloading file...");
                www.timeout = DownloadTimeout;
                await www.SendWebRequest();

                if (www.result == webResult.ConnectionError || www.result == webResult.ProtocolError)
                {
                    LogWarning("Error downloading: " + www.error);
                    www = null;
                    return null;
                }

                data = www.downloadHandler.text;
                if (string.IsNullOrEmpty(data))
                    LogWarning("Download contains empty response");

            }

            www = null;
            return data;
        }


        private string GetCached()
        {
            if (System.IO.File.Exists(file))
            {
                Log("Cache file exist");
                try
                {

                    var created = System.IO.File.GetCreationTimeUtc(file);
                    var age = System.DateTime.UtcNow - created;
                    Log($"Cache file create at: {created}. Age: {age.TotalHours} hours");

                    if (age > CacheTimeout)
                    {
                        Log("Cached file expired");
                    }
                    else
                    {
                        var data = System.IO.File.ReadAllText(file);
                        if (string.IsNullOrEmpty(file))
                            LogWarning($"Cache file '{file}' ({url}) is empty");
                        else
                            return data;
                    }
                }
                catch (System.Exception e)
                {
                    LogWarning($"Error reading to cache file '{file}' ({url}):\n{e.Message}");
                }
            }
            else
            {
                Log("No cache file found");
            }

            return null;
        }


        public async void Execute()
        {
            await ExecuteAsync();
            OnCompleted.Invoke(this);
            Destroy();
        }

        public async System.Threading.Tasks.Task<string> ExecuteAsync()
        {
            if (enableCached && (Result = GetCached()) != null)
            {
                Debug.Log("Got cached");
                return Result;
            }


            if (enableDownload && (Result = await Download()) != null)
            {
                if (enableCached)
                    SaveToCache(Result);
                Debug.Log("Downloaded");
                return Result;
            }


            if (embeddedResource == null)
                return null;

            Log("Using embedded file");
            var asset = await Resources.LoadAsync<TextAsset>(embeddedResource);
            if (asset != null)
            {
                Result = (asset as TextAsset)?.text;
            }
            else
            {
                LogWarning("Embedded file cannot be loaded");
            }

            return Result;
        }


        public void Destroy()
        {
            OnCompleted.RemoveAllListeners();
            Result = null;
            if (www != null)
            {
                www.Abort();
                www = null;
            }
        }


        static void Log(string text)
        {
            if (EnableLogging)
                Debug.Log("#WWWCache# " + text);
        }

        static void LogWarning(string text)
        {
            if (EnableLogging)
                Debug.LogWarning("#WWWCache# " + text);
        }

    }
}