using UnityEngine;

namespace TrickyTribe.Parser
{

    [CreateAssetMenu(fileName = "TSVSettings", menuName = "TSV/TSVSettings", order = 1)]
    public class TSVSettings : ScriptableObject
    {
        public string sheetId = "10TweAozjqLTvNTnmP4hu6kn8HeBY_FmsKH_8iUibZeo";
        public string url = "https://tifm.trickytribe.tfsnw.net/googleproxy/proxy.php?id={0}&sheet={1}&action=dev";

        public string GetURL(string sheetName)
        {
            return string.Format(url, sheetId, sheetName);
        }
    }
}