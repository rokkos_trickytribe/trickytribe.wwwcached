﻿using System.Collections;
using UnityEngine;
using System.Linq;
using webResult = UnityEngine.Networking.UnityWebRequest.Result;

namespace TrickyTribe.Parser
{
    public static class TSVDownloader
    {

#if UNITY_EDITOR
        [UnityEditor.MenuItem("Tricky Tribe/TSV/Download Data", false, 300)]
        static void DownloadSpreadsheet()
        {
            var tsv = Resources.Load<TSVSettings>("TSVSettings");
            if (tsv == null)
            {
                Debug.LogError("Cannot find 'TSVSettings' in Resources folder...");
                return;

            }

            Debug.Log("DOWNLOAD STARTED");

            var settings = "SETTINGS";

            var enumSettings = Download(tsv.GetURL(settings), settings);
            enumSettings.MoveNext();
            while (!((UnityEngine.Networking.UnityWebRequestAsyncOperation)enumSettings.Current).isDone) ;
            enumSettings.MoveNext();


            var file = System.IO.Path.Combine(System.IO.Path.Combine(Application.dataPath, "Resources"),
                    settings + ".txt");
            if (!System.IO.File.Exists(file))
            {
                Debug.LogWarning("Cannot parse settings because settings file does not exist: " + file);
                return;
            }

            var sheets = TSVParser.GetSheets(System.IO.File.ReadAllText(file));
            if (sheets == null)
                return;
            var files = sheets.ToArray();

            for (var i = 0; i < files.Length; i++)
            {
                var enumerator = Download(tsv.GetURL(files[i]), files[i]);
                enumerator.MoveNext();
                //Debug.Log(enumerator.Current);
                while (!((UnityEngine.Networking.UnityWebRequestAsyncOperation)enumerator.Current).isDone) ;
                enumerator.MoveNext();
            }

            Debug.Log("DOWNLOAD FINISHED");


            /*
            file = System.IO.Path.Combine(System.IO.Path.Combine(Application.dataPath, "Resources"), "TRANSLATIONS.txt");
            if (!System.IO.File.Exists(file))
            {
                Debug.LogWarning("Cannot update character lists, because translation file does not exist: " + file);
            }
            else
            {
                UpdateCharacterLists(System.IO.File.ReadAllText(file));
                Debug.Log("CHARACTER LISTS UPDATED");
            }*/
        }

        static IEnumerator Download(string url, string file)
        {
            file = System.IO.Path.Combine(System.IO.Path.Combine(Application.dataPath, "Resources"), file + ".txt");
            using (var www = UnityEngine.Networking.UnityWebRequest.Get(url))
            {
                yield return www.SendWebRequest();

                if (www.result == webResult.ConnectionError || www.result == webResult.ProtocolError)
                {
                    Debug.LogWarning("Error downloading: " + www.error);
                    yield break;
                }

                var data = www.downloadHandler.text;
                if (string.IsNullOrEmpty(data))
                    Debug.Log("Download contains empty response");

                try
                {
                    Debug.Log("Saving to file: " + file);
                    // create required folders
                    new System.IO.FileInfo(file).Directory.Create();
                    // write to disk
                    System.IO.File.WriteAllText(file, data);
                    System.IO.File.SetCreationTimeUtc(file, System.DateTime.UtcNow);

                    UnityEditor.AssetDatabase.Refresh();
                }
                catch (System.Exception e)
                {
                    Debug.LogWarning($"Error saving to file '{file}' ({url}):\n{e.Message}");
                }
            }
        }

        /* public static void UpdateCharacterLists(string dataTSV)
         {
             var hash = new HashSet<char>();
             var common = " 0123456789-%+():;.,&*#@!?<>=/|~`±{}_[]'®\" $£€";
             var dict = new Dictionary<string, string[]>
             {
                 {
                     "LATIN",
                     new[]
                     {
                         common +
                         "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM abcçdefgğhıijklmnoöprsştuüvyzABCÇDEFGĞHIİJKLMNOÖPRSŞTUÜVYZ ŒÀÂÇÈÉÊËÎÏÔÙÛÜœàâçèéêëîïôùûü ÄäÖöÜüẞß ñ¿¡ ñ¿¡",
                         "EN", "FR", "DE", "IT", "PT-BR", "ES-ES", "TR"
                     }
                 },
                 {"ZH-TW", new[] {common, "ZH-TW"}},
                 {"ZH-CN", new[] {common, "ZH-CN"}},
                 {"JP", new[] {common, "JP"}},
                 {"KO", new[] {common, "KO"}},
                 {"RU", new[] {common + "лвАаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя", "RU"}}
             };
     
             var list = new List<String>();
             foreach (var key in dict.Keys)
             {
                 hash.Clear();
                 list.Add(dict[key][0]);
     
                 for (var i = 1; i < dict[key].Length; i++)
                 {
                     TSVParser.Parse(
                         dataTSV,
                         list,
                         (TSVParser.Line line, object payload) =>
                         {
                             // *** ignore fortune notifications
                             var id = line.Get("ID", null);
                             if (id != null && (id.StartsWith("fortune_title_") || id.StartsWith("fortune_body_")))
                                 return null;
                             // ***
                             return line.Get((string) payload, null) ?? line.Get(Translations.DEFAULT_LANGUAGE, null);
                         },
                         dict[key][i]);
                 }
     
                 foreach (var line in list)
                 {
                     if (string.IsNullOrEmpty(line)) continue;
                     Debug.Log("line: "+line);
                     foreach (var chr in line) hash.Add(chr);
                     foreach (var chr in line.ToUpper()) hash.Add(chr);
                 }
     
                 list.Clear();
     
                 Debug.Log($"Unique characters for {key}: {hash.Count}");
                 var file = System.IO.Path.Combine(System.IO.Path.Combine(Application.dataPath, "Images/Fonts"),
                     $"{key}.txt");
                 try
                 {
                     new System.IO.FileInfo(file).Directory.Create();
                     // write to disk
                     System.IO.File.WriteAllText(file, string.Join("", hash.ToArray()));
                     System.IO.File.SetCreationTimeUtc(file, DateTime.UtcNow);
                     UnityEditor.AssetDatabase.Refresh();
                 }
                 catch
                 {
                 }
             }
         }
         */
#endif
    }
}