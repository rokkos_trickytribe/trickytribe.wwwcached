﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Unity.Mathematics;
using UnityEngine;
using TrickyTribe.Collections;

namespace TrickyTribe.Parser
{

    public static class TypeExtensions
    {
        public static bool IsImplementationOf(this Type baseType, Type interfaceType)
        {
            return baseType.GetInterfaces().Any(interfaceType.Equals);
        }



    }

    public class TSVParser
    {
        public interface ITSVParsable
        {
            bool Parse(string str);
        }

        [AttributeUsage(AttributeTargets.Field)]
        public class Entry : Attribute
        {
            public string Id = null;
            public bool RequireValue = false;
            public string Format = null;

            public Entry()
            {
            }
            public Entry(string id)
            {
                Id = id;
            }
            public Entry(bool requireValue)
            {
                RequireValue = requireValue;
            }
            public Entry(string id, bool requireValue)
            {
                Id = id;
                RequireValue = requireValue;
            }
        }
        [AttributeUsage(AttributeTargets.Class)]
        public class Filter : Attribute
        {
            public string Id;
            public Filter(string id)
            {
                Id = id;
            }
        }


        public class Header
        {
            private Dictionary<string, int> dict;

            public int Length = 0;

            public Header(string line)
            {
                var header = line.Split('\t');
                Length = header.Length;

                dict = new Dictionary<string, int>(Length);
                for (var i = 0; i < header.Length; i++)
                {
                    if (string.IsNullOrEmpty(header[i]))
                        continue;
                    dict.Add(header[i], i);
                }
            }

            public bool GetIdx(string key, out int idx)
            {
                if (!dict.TryGetValue(key, out idx))
                {
                    return false;
                    //  Debug.Log($"Cannot find column '{key}'");
                }
                return true;
            }
        }

        public class Line
        {
            private string[] line;
            public Header header;
            private int lineLength;

            public Line(string[] line, Header header)
            {
                this.line = line;
                lineLength = line.Length;
                this.header = header;
            }

            #region Getters


            public string Get(string key, string def)
            {
                if (!header.GetIdx(key, out var idx) || idx >= lineLength)
                    return def;
                var ret = line[idx];
                return string.IsNullOrEmpty(ret) ? def : ret;
            }

            public bool Get(string key, bool def)
            {
                var s = Get(key, null);
                if (s == null) return def;
                return s.Equals("true", StringComparison.OrdinalIgnoreCase);
            }

            public float Get(string key, float def)
            {
                var s = Get(key, null);
                if (s == null) return def;
                return float.TryParse(s, NumberStyles.Float, CultureInfo.InvariantCulture, out var val) ? val : def;
            }

            public int Get(string key, int def)
            {
                var s = Get(key, null);
                if (s == null) return def;
                return int.TryParse(s, out var val) ? val : def;
            }


            public float3 Get(string key, float3 def)
            {
                var s = Get(key, null);
                if (s == null) return def;
                var str = s.Split(',');
                float.TryParse(str[0], NumberStyles.Float, CultureInfo.InvariantCulture, out def.x);
                float.TryParse(str[1], NumberStyles.Float, CultureInfo.InvariantCulture, out def.y);
                float.TryParse(str[2], NumberStyles.Float, CultureInfo.InvariantCulture, out def.z);
                return def;
            }
            public float2 Get(string key, float2 def)
            {
                var s = Get(key, null);
                if (s == null) return def;
                var str = s.Split(',');
                float.TryParse(str[0], NumberStyles.Float, CultureInfo.InvariantCulture, out def.x);
                float.TryParse(str[1], NumberStyles.Float, CultureInfo.InvariantCulture, out def.y);
                return def;
            }

            public int3 Get(string key, int3 def)
            {
                var s = Get(key, null);
                if (s == null) return def;
                var str = s.Split(',');
                int.TryParse(str[0], NumberStyles.Float, CultureInfo.InvariantCulture, out def.x);
                int.TryParse(str[1], NumberStyles.Float, CultureInfo.InvariantCulture, out def.y);
                int.TryParse(str[2], NumberStyles.Float, CultureInfo.InvariantCulture, out def.z);
                return def;
            }
            public int2 Get(string key, int2 def)
            {
                var s = Get(key, null);
                if (s == null) return def;
                var str = s.Split(',');
                int.TryParse(str[0], NumberStyles.Float, CultureInfo.InvariantCulture, out def.x);
                int.TryParse(str[1], NumberStyles.Float, CultureInfo.InvariantCulture, out def.y);
                return def;
            }

            public Color Get(string key, Color def)
            {
                var s = Get(key, null);
                if (s == null) return def;
                return ColorUtility.TryParseHtmlString(s, out var result) ? result : def;
            }


            public Quaternion Get(string key, Quaternion def)
            {
                var s = Get(key, null);
                if (s == null) return def;
                var str = s.Split(',');
                if (str.Length < 3) return def;
                return Quaternion.Euler(
                    float.Parse(str[0], CultureInfo.InvariantCulture),
                    float.Parse(str[1], CultureInfo.InvariantCulture),
                    float.Parse(str[2], CultureInfo.InvariantCulture)
                );
            }

            public T Get<T>(string key, T def) where T : struct, IConvertible
            {
                if (!header.GetIdx(key, out var idx) || idx >= lineLength)
                    return def;
                return Enum.TryParse(line[idx], true, out T result) ? result : def;
            }

            public int[] GetIntArray(string key)
            {
                var s = Get(key, null);
                if (s == null) return null;

                var str = s.Split(',');
                var ret = new int[str.Length];
                for (var i = 0; i < str.Length; i++)
                    int.TryParse(str[i], NumberStyles.Float, CultureInfo.InvariantCulture, out ret[i]);
                return ret;
            }
            public float[] GetFloatArray(string key)
            {
                var s = Get(key, null);
                if (s == null) return null;
                var str = s.Split(',');
                var ret = new float[str.Length];
                for (var i = 0; i < str.Length; i++)
                    float.TryParse(str[i], NumberStyles.Float, CultureInfo.InvariantCulture, out ret[i]);
                return ret;
            }

            #endregion

            public override string ToString()
            {
                return $"[Line '{string.Join(", ", line)}']";
            }
        }


        public delegate T ParseDelegate<T>(Line line, object payload = null);



        public static HashSet<string> GetSheets(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                Debug.LogWarning("Cannot parse data. Text is empty or null: " + text);
                return null;
            }

            var lines = text.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

            if (lines.Length <= 2)
                return null;


            var header = lines[1].Split('\t');
            var idKey = -1;
            for (var i = 0; i < header.Length; i++)
            {
                if (header[i].Equals("KEY", StringComparison.OrdinalIgnoreCase))
                    idKey = i;
            }
            if (idKey < 0) return null;

            var list = new HashSet<string>();

            for (var i = 2; i < lines.Length; i++)
            {
                if (string.IsNullOrEmpty(lines[i]))
                    continue;

                try
                {
                    var ls = lines[i].Split('\t');
                    if (ls == null || ls.Length <= idKey)
                        continue;

                    if (!ls[idKey].StartsWith("SHEET_", StringComparison.InvariantCultureIgnoreCase))
                        continue;


                    for (var j = 0; j < ls.Length; j++)
                    {
                        if (j == idKey || j >= header.Length || string.IsNullOrEmpty(header[j]) || string.IsNullOrEmpty(ls[j])) continue;
                        list.Add(ls[j]);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogWarning("Error parsing data @id=" + i + ": " + e.Message);
                }
            }
            return list;

        }




        public static bool ParseSettings<T>(string text, T obj, string baseColumn = "VALUE", List<string> overrideColumns = null)
        {
            if (string.IsNullOrEmpty(text))
            {
                Debug.LogWarning("Cannot parse data. Text is empty or null: " + text);
                return false;
            }
            if (string.IsNullOrEmpty(baseColumn))
            {
                Debug.LogWarning("Cannot parse data. No column specified");
                return false;
            }

            var lines = text.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

            if (lines.Length <= 2)
                return false;

            if (overrideColumns != null)
            {
                for (var i = 0; i < overrideColumns.Count; i++)
                    overrideColumns[i] = overrideColumns[i].ToUpperInvariant();
            }

            var header = lines[1].Split('\t');
            var idKey = -1;
            var idBase = -1;
            var idOverrides = new List<int>();
            for (int i = 0; i < header.Length; i++)
            {
                if (header[i].Equals("KEY", StringComparison.OrdinalIgnoreCase))
                    idKey = i;
                if (header[i].Equals(baseColumn, StringComparison.OrdinalIgnoreCase))
                    idBase = i;

                if (overrideColumns != null && overrideColumns.Contains(header[i].ToUpperInvariant()))
                    idOverrides.Add(i);
            }
            if (idKey < 0 || idBase < 0) return false;



            var fields = typeof(T).GetFields();
            var dict = new Dictionary<string, FieldInfo>();
            foreach (var fieldInfo in fields)
            {
                var pf = fieldInfo.GetCustomAttribute<Entry>();
                if (pf == null) continue;
                dict.Add(pf.Id, fieldInfo);
            }

            for (var i = 2; i < lines.Length; i++)
            {
                if (string.IsNullOrEmpty(lines[i]))
                    continue;

                try
                {
                    var ls = lines[i].Split('\t');
                    if (ls == null || ls.Length <= idKey || ls.Length <= idBase)
                        continue;

                    if (!dict.TryGetValue(ls[idKey], out var fieldInfo))
                        continue;

                    string str = null;
                    for (var j = 0; j < idOverrides.Count; j++)
                    {
                        var idOverride = idOverrides[j];
                        if (idOverride >= 0 && idOverride < ls.Length && !string.IsNullOrEmpty(ls[idOverride]))
                            str = ls[idOverride];
                    }
                    if (string.IsNullOrEmpty(str))
                        str = ls[idBase];

                    if (TryGetValue(obj, fieldInfo, str, null, out var val))
                        fieldInfo.SetValue(obj, val);
                }
                catch (Exception e) { Debug.LogWarning("Error parsing data @id=" + i + ": " + e.Message); }
            }
            return true;
        }



        public static bool Parse<T>(string text, ICollection<T> list, ParseDelegate<T> parser, object payload = null)
        {
            if (string.IsNullOrEmpty(text))
            {
                Debug.LogWarning("Cannot parse data. Text is empty or null: " + text);
                return false;
            }

            var lines = text.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

            if (lines.Length <= 2)
                return false;

            var header = new Header(lines[1]);

            //list?.Clear();
            for (int i = 2; i < lines.Length; i++)
            {
                if (string.IsNullOrEmpty(lines[i]))
                    continue;

                var bd = default(T);
                try
                {
                    var ls = lines[i].Split('\t');
                    if (ls == null || ls.Length == 0)
                        continue;
                    var line = new Line(ls, header);
                    bd = parser(line, payload);
                }
                catch (Exception e)
                {
                    Debug.LogWarning("Error parsing data @id=" + i + ": " + e.Message);
                }
                if (bd == null)
                    continue;
                list?.Add(bd);
            }
            return true;
        }




        // if (float.TryParse(s[0], NumberStyles.Float, CultureInfo.InvariantCulture, out val.x) &&
        public static T ParseAttributes<T>(Line line, object payload = null) where T : class, new()
        {
            var fields = typeof(T).GetFields();
            var f = typeof(T).GetCustomAttribute<Filter>();
            if (f != null)
            {
                if (!line.Get(f.Id, false))
                {
                    //Debug.Log($"Skipping entry because {f.Id} = FALSE @ {typeof(T)} : {line}");
                    return null;
                }
            }

            var bd = new T();
            foreach (var fieldInfo in fields)
            {
                var attribute = fieldInfo.GetCustomAttribute<Entry>();
                if (attribute == null) continue;

                var colId = attribute.Id ?? fieldInfo.Name;
                if (attribute.Id == null)
                {
                    Debug.Log("No 'Id' defined on field " + fieldInfo.Name);
                }

                var dataStr = line.Get(colId, null);
                if (dataStr == null)
                {
                    if (attribute.RequireValue)
                    {
                        Debug.Log("Value not defined on column: " + colId);
                        return null;
                    }
                    continue;
                }


                if (TryGetValue(bd, fieldInfo, dataStr, attribute, out var val))
                    fieldInfo.SetValue(bd, val);
            }
            return bd;
        }

        static bool TryGetValue<T>(T obj, FieldInfo fieldInfo, string str, Entry attribute, out object value)
        {
            var valCurrent = fieldInfo.GetValue(obj);


            // Debug.Log(valCurrent?.GetType() + "," + valCurrent?.GetType().IsGenericType);

            if (fieldInfo.FieldType == typeof(string))
            {
                if (attribute == null)
                    attribute = fieldInfo.GetCustomAttribute<Entry>();
                value = attribute != null && attribute.Format != null ? string.Format(attribute.Format, str) : str;
                return true;
            }
            else if (fieldInfo.FieldType == typeof(float))
            {
                if (float.TryParse(str, NumberStyles.Float, CultureInfo.InvariantCulture, out var val))
                {
                    value = val;
                    return true;
                }
            }
            else if (fieldInfo.FieldType == typeof(int))
            {
                if (int.TryParse(str, NumberStyles.Float, CultureInfo.InvariantCulture, out var val))
                {
                    value = val;
                    return true;
                }
            }
            else if (fieldInfo.FieldType == typeof(bool))
            {
                value = str.Equals("true", StringComparison.OrdinalIgnoreCase);
                return true;
            }
            else if (fieldInfo.FieldType == typeof(double))
            {
                if (double.TryParse(str, NumberStyles.Float, CultureInfo.InvariantCulture, out var val))
                {
                    value = val;
                    return true;
                }
            }
            else if (fieldInfo.FieldType == typeof(Range))
            {
                var s = str.Split(',');
                if (s.Length == 1)
                {
                    var val = new Range();
                    if (float.TryParse(s[0], NumberStyles.Float, CultureInfo.InvariantCulture, out val.min))
                    {
                        val.max = val.min;
                        value = val;
                        return true;
                    }
                }
                else if (s.Length >= 2)
                {
                    var val = new Range();
                    if (float.TryParse(s[0], NumberStyles.Float, CultureInfo.InvariantCulture, out val.min) && float.TryParse(s[1], NumberStyles.Float, CultureInfo.InvariantCulture, out val.max))
                    {
                        value = val;
                        return true;
                    }
                }
            }
            else if (fieldInfo.FieldType == typeof(MaterialProp))
            {
                var s = str.Split(',');
                if (s.Length == 1)
                {
                    var val = new MaterialProp();
                    if (ColorUtility.TryParseHtmlString(s[0], out val.color))
                    {
                        value = val;
                        return true;
                    }
                }
                else if (s.Length >= 2)
                {
                    var val = new MaterialProp();
                    if (ColorUtility.TryParseHtmlString(s[0], out val.color) && ColorUtility.TryParseHtmlString(s[1], out val.emission))
                    {
                        value = val;
                        return true;
                    }
                }
            }



            else if (valCurrent is ITSVParsable parsable)        // shortcut for below: if implementation of ITSVParsable is value type, or if default object is created, reuse it
            {
                value = parsable;
                return parsable.Parse(str);
            }
            else if (fieldInfo.FieldType.IsImplementationOf(typeof(ITSVParsable)))        // if field is null, create new object
            {
                var val = (ITSVParsable)fieldInfo.FieldType.GetConstructor(Type.EmptyTypes).Invoke(null);
                value = val;
                return val.Parse(str);
            }



            /*parsableCollection.Clear();
            var s = str.Split(';');
            if (s.Length > 0)
            {
                for (var i = 0; i < s.Length; i++)
                {
                }
               
                var val = (ITSVParsable)fieldInfo.FieldType.GetConstructor(Type.EmptyTypes).Invoke(null);
                
                    int.TryParse(s[i], NumberStyles.Float, CultureInfo.InvariantCulture, out val[i]);
                value = val;
                return true;
            }*/




            else if (fieldInfo.FieldType == typeof(WeightedList))
            {
                var s = str.Split(',');
                if (s.Length > 0)
                {
                    var list = new WeightedList();
                    for (var i = 0; i < s.Length; i++)
                    {
                        var ss = s[i].Split(':');
                        if (ss.Length == 0) continue;

                        var id = ss[0];
                        if (string.IsNullOrEmpty(id))
                            continue;

                        var weight = 100;
                        if (ss.Length >= 2)
                        {
                            if (!int.TryParse(ss[1], out weight))
                                weight = 100;
                        }
                        list.items.Add(new WeightedList.Item { weight = weight, id = id });
                    }
                    value = list;
                    return true;
                }


            }
            else if (fieldInfo.FieldType == typeof(Vector2))
            {
                var s = str.Split(',');
                if (s.Length >= 2)
                {
                    var val = new Vector2();
                    if (float.TryParse(s[0], NumberStyles.Float, CultureInfo.InvariantCulture, out val.x) && float.TryParse(s[1], NumberStyles.Float, CultureInfo.InvariantCulture, out val.y))
                    {
                        value = val;
                        return true;
                    }
                }
            }
            else if (fieldInfo.FieldType == typeof(Vector3))
            {
                var s = str.Split(',');
                if (s.Length >= 3)
                {
                    var val = new Vector3();
                    if (float.TryParse(s[0], NumberStyles.Float, CultureInfo.InvariantCulture, out val.x) && float.TryParse(s[1], NumberStyles.Float, CultureInfo.InvariantCulture, out val.y) && float.TryParse(s[2], NumberStyles.Float, CultureInfo.InvariantCulture, out val.z))
                    {
                        value = val;
                        return true;
                    }
                }
            }
            else if (fieldInfo.FieldType == typeof(float2))
            {
                var s = str.Split(',');
                if (s.Length >= 2)
                {
                    var val = new float2();
                    if (float.TryParse(s[0], NumberStyles.Float, CultureInfo.InvariantCulture, out val.x) && float.TryParse(s[1], NumberStyles.Float, CultureInfo.InvariantCulture, out val.y))
                    {
                        value = val;
                        return true;
                    }
                }
            }
            else if (fieldInfo.FieldType == typeof(float3))
            {
                var s = str.Split(',');
                if (s.Length >= 3)
                {
                    var val = new float3();
                    if (float.TryParse(s[0], NumberStyles.Float, CultureInfo.InvariantCulture, out val.x) && float.TryParse(s[1], NumberStyles.Float, CultureInfo.InvariantCulture, out val.y) && float.TryParse(s[2], NumberStyles.Float, CultureInfo.InvariantCulture, out val.z))
                    {
                        value = val;
                        return true;
                    }
                }
            }
            else if (fieldInfo.FieldType == typeof(float4))
            {
                var s = str.Split(',');
                if (s.Length >= 4)
                {
                    var val = new float4();
                    if (float.TryParse(s[0], NumberStyles.Float, CultureInfo.InvariantCulture, out val.x) && float.TryParse(s[1], NumberStyles.Float, CultureInfo.InvariantCulture, out val.y) && float.TryParse(s[2], NumberStyles.Float, CultureInfo.InvariantCulture, out val.z) && float.TryParse(s[3], NumberStyles.Float, CultureInfo.InvariantCulture, out val.w))
                    {
                        value = val;
                        return true;
                    }
                }
            }
            else if (fieldInfo.FieldType == typeof(int2))
            {
                var s = str.Split(',');
                if (s.Length >= 2)
                {
                    var val = new int2();
                    if (int.TryParse(s[0], NumberStyles.Float, CultureInfo.InvariantCulture, out val.x) && int.TryParse(s[1], NumberStyles.Float, CultureInfo.InvariantCulture, out val.y))
                    {
                        value = val;
                        return true;
                    }
                }
            }
            else if (fieldInfo.FieldType == typeof(int3))
            {
                var s = str.Split(',');
                if (s.Length >= 3)
                {
                    var val = new int3();
                    if (int.TryParse(s[0], NumberStyles.Float, CultureInfo.InvariantCulture, out val.x) &&
                        int.TryParse(s[1], NumberStyles.Float, CultureInfo.InvariantCulture, out val.y) &&
                        int.TryParse(s[2], NumberStyles.Float, CultureInfo.InvariantCulture, out val.z))
                    {
                        value = val;
                        return true;
                    }
                }
            }
            else if (fieldInfo.FieldType == typeof(Color))
            {
                if (ColorUtility.TryParseHtmlString(str, out var val))
                {
                    value = val;
                    return true;
                }
            }

            else if (fieldInfo.FieldType == typeof(int[]))
            {
                var s = str.Split(',');
                if (s.Length > 0)
                {
                    var val = new int[s.Length];
                    for (var i = 0; i < s.Length; i++)
                        int.TryParse(s[i], NumberStyles.Float, CultureInfo.InvariantCulture, out val[i]);
                    value = val;
                    return true;
                }
            }
            else if (fieldInfo.FieldType == typeof(float[]))
            {
                var s = str.Split(',');
                if (s.Length > 0)
                {
                    var val = new float[s.Length];
                    for (var i = 0; i < s.Length; i++)
                        float.TryParse(s[i], NumberStyles.Float, CultureInfo.InvariantCulture, out val[i]);
                    value = val;
                    return true;
                }
            }
            else
            {
                if (fieldInfo.FieldType.IsEnum)
                {
                    if (Enum.IsDefined(fieldInfo.FieldType, str))
                    {

                        value = Enum.Parse(fieldInfo.FieldType, str);
                        return true;
                    }
                    else
                    {
                        Debug.LogWarning($"Illegal ENUM value '{str}', field name {fieldInfo.Name}");
                    }
                }
                else
                {
                    Debug.LogError("Not supported on " + fieldInfo.FieldType);
                }
            }
            value = null;
            return false;
        }






    }
}
